#! /bin/bash -e

# Add Jenkins to the 'docker' group so that it can have access to docker.
DOCKER_GID=$(ls -aln /var/run/docker.sock  | awk '{print $4}')
echo "$(whoami) creating group $DOCKER_GID and adding Jenknins"
if [ grep -q "^${hostdocker}:" /etc/group ]; then
    groupadd -g $DOCKER_GID hostdocker
    usermod -a -G hostdocker jenkins
fi

: ${JENKINS_HOME:="/var/jenkins_home"}
touch "${COPY_REFERENCE_FILE_LOG}" || (echo "Can not write to ${COPY_REFERENCE_FILE_LOG}. Wrong volume permissions?" && exit 1)
echo "--- Copying files at $(date)" >> "$COPY_REFERENCE_FILE_LOG"
find /usr/share/jenkins/ref/ -type f -exec bash -c ". /usr/local/bin/jenkins-support; copy_reference_file '{}'" \;

# if `docker run` first argument start with `--` the user is passing jenkins launcher arguments
if [[ $# -lt 1 ]] || [[ "$1" == "--"* ]]; then
  eval "exec java $JAVA_OPTS -jar /usr/share/jenkins/jenkins.war $JENKINS_OPTS \"\$@\""
fi

# As argument is not jenkins, assume user want to run his own process, for sample a `bash` shell to explore this image
exec "$@"
