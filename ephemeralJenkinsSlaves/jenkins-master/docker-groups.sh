#! /bin/bash -e
DOCKER_GID=$(ls -aln /var/run/docker.sock  | awk '{print $4}')

if [ "$DOCKER_GID" ]; then
    groupadd -g $DOCKER_GID hostdocker
    usermod -a -G hostdocker jenkins
fi

exec "$@"