# Ephemeral Build Slaves using Jenkins 2.7 and Docker 1.11

This repository contains all the files necessary to create a jenkins docker environment. Please check this [repository](https://bitbucket.org/continuousdeliveryintertec/dockerlab/src/1493cf302e263e4f2e105e956f759fd403b715e5/registry/) if you want to create a private docker registry.

The build master and slaves are designed to be docker containers in this configuration. Please check this [PDF](deploymentStrategies.pdf) for more details about the suggested deployment strategies using this setup.

![Jenkins](images/dockerJenkins.png)

# Quick Start

To get this up and running as quickly as possible here's the steps you need to do is:

Make sure you have all the pre-reqs installed (i.e. Docker-engine 1.11):

- [CentOS 7.2](#centos-7.2)
- [Ubuntu 14.04](#ubuntu-14.04)

Clone this repository and configure the Jenkins Master:

- [Jenkins Settings](#jenkins-settings)

If everything is configured correctly your job should dynamically allocate a slave, run itself and then de-allocate the slave.

## CentOS 7.2

1.  Install Docker 1.11
    1. `sudo yum -y -q update`
    2. ```sudo tee /etc/yum.repos.d/docker.repo &lt;&lt;-&#39;EOF&#39;
       [dockerrepo]
       name=Docker Repository
       baseurl=https://yum.dockerproject.org/repo/main/centos/7/
       enabled=1
       gpgcheck=1
       gpgkey=https://yum.dockerproject.org/gpg
       EOF``` Do not copy the command above from the MD format, use the raw file format instead.
       ```
    3. `sudo yum -y -q install docker-engine-1.11.2`
    4. `sudo chkconfig docker on`
    5. `sudo service docker start`
    6. `sudo usermod -aG docker <user-name>`
    7. `sudo systemctl edit docker` and add the following:
    ```[Service]
    ExecStart=
    ExecStart=/usr/bin/docker daemon -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock``` Do not copy the configuration above from the MD format, use the raw file format instead.
    8. Save without changing the default file name.
    9. `sudo systemctl restart docker`
    10. `sudo systemctl status docker`
    ```
2.  Install Docker Compose
    1. `sudo su`
    2. ```sudo curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose```
    3. `chmod +x /usr/bin/docker-compose`
3.  Install git
    1. `sudo yum -y -q install git`

## Ubuntu 14.04
1. Install Docker 1.11
    1. `sudo apt-get install apt-transport-https ca-certificates`
    2. `apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D`
    3. `mkdir -p /etc/apt/sources.list.d`
    4. `echo deb https://apt.dockerproject.org/repo ubuntu-trusty main > /etc/apt/sources.list.d/docker.list`
    5. `apt-get update`
    6. `sudo apt-get purge lxc-docker`
    7. `sudo apt-get install linux-image-extra-$(uname -r)`
    8. `sudo apt-get install -y -q docker-engine=1.11.2-0~trusty`
    9. Add `DOCKER_OPTS='-H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock'` to /etc/default/docker
2.  Install Docker Compose
    1. `sudo su`
    2. ```sudo curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose```
    3. `chmod +x /usr/bin/docker-compose`
3. Install Make
    1. `sudo apt-get install make`

## Jenkins settings

1. Clone this repository
2. Copy your authorized_keys to the jenkins-master folder
    1. `mkdir jenkins-master/certs`
    2. `cp -R /home/<user-name>/.ssh/authorized_keys jenkins-master/certs`
3. `make build`
4. `make run`
5. `docker logs jenkins_master_1`
6. Copy the default password generated
     ![Jenkins](images/jenkinsSetup.png)
7. Point your browser to `http://yourdockermachineip` and paste the password and press continue.
8. In jenkins, go to Manage Plugins and make sure that the following plugins get installed:
    1. Bitbucket Plugin
    2. Docker Plugin
    3. Cloudbees Docker Plugin
    4. Install ssh-agent
9. In jenkins, configure a ssh-key pair using an ssh private key for the user "jenkins". As the private key, use the file in the repository folder jenkins-slave/files/dummy\_private\_rsa\_key (Passphrase: Puravida2016!)
     ![Jenkins](images/tutorial7_jenkinscertsetup1.gif)
10. In jenkins, configure a Docker Certificates Directory credential using /usr/local/etc/jenkins/certs as the source directory
    ![Jenkins](images/tutorial7_jenkinscertsetup2.gif)
11. In jenkins configuration, add a new Docker cloud provider
    1. Set host to `https://yourdockermachineip:2376`
    2. Select the docker certificates directory you made
    3. Set read timeout to 5
    4. set connection timeout to 15
    5. Click on "Test Connection" and make sure you get a valid response
       ![Jenkins](images/tutorial7_jenkinsdockersetup1.gif)
12. In jenkins config on your new Docker cloud add a Docker template
    1. Set the image name to: jenkins\_slave
    2. Create a label "testslave"
    3. Make sure credentials are your new ssh key pair you made above
    4. Click "Save"
       ![Jenkins](images/tutorial7_jenkinsdockersetup2.gif)
13. Create a new Pipeline jenkins job
    1. Add the pipeline script below
    2. Save job
    3. Run job

```
#!groovy

    node('testslave') { 
        currentBuild.result = "SUCCESS" 
        
        stage 'Checkout' 
        sh 'echo "Checkout"' 
        
        stage 'Test' 
        sh 'echo "Test"' 
    
    } 
    node() { 
        currentBuild.result = "SUCCESS" 
        
        stage 'Checkout' 
        sh 'echo "Checkout"' 
        
        stage 'Test' 
        sh 'echo "Deploy"' 
        
        stage 'Run Container' 
        docker.image('hello-world').run() 
    }
```

> This setup was inspired by the following article: http://engineering.riotgames.com/news/building-jenkins-inside-ephemeral-docker-container (Recomended Read)

