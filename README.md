# Jenkins Tutorials and Labs

This repo contains [Jenkins](https://jenkins.io/) labs and tutorials authored by members of the community. We welcome contributions and want to grow the repo.

### About Jenkins Pipelines
- [Introduction to Pipelines](https://jenkins.io/doc/book/pipeline/)
- [Pipeline steps reference](https://jenkins.io/doc/pipeline/steps/)
- [Pipeline examples](https://jenkins.io/doc/pipeline/examples/)

#### Jenkins tutorials:
* [Ephemeral Build Slaves](ephemeralJenkinsSlaves/)
* [Beginner](beginner/)

#### Contributing
We want to see this repo grow, so if you have a tutorial to submit please see this guide:

[Guide to submitting your own tutorial](contribute.md)